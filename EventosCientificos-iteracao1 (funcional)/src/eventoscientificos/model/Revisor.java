/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.model;

/**
 *
 * @author Nuno Silva
 */
public class Revisor
{
    private String m_strNome;
    private Utilizador m_utilizador;
    
    public Revisor(Utilizador u)
    {
        m_strNome = u.getNome();
        m_utilizador = u;
    }

    public boolean valida()
    {
        return true;
    }

    public String getNome()
    {
        return m_strNome;
    }
    
    public Utilizador getUtilizador()
    {
        return m_utilizador;
    }
    
    @Override
    public String toString()
    {
        return m_utilizador.toString();
    }
}
