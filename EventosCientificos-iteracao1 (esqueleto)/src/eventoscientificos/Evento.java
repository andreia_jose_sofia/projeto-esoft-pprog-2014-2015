/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos;

/**
 *
 * @author Nuno Silva
 */
public class Evento
{
    private String m_strTitulo;
    private String m_strDescricao;
    private Local m_local;
    private String m_strDataInicio;
    private String m_strDataFim;
    private CP m_cp;

    public Evento()
    {
        m_local = new Local();
    }
    
    public CP novaCP()
    {
        m_cp = new CP();
        
        return m_cp;
    }
    
    public void setCP(CP cp)
    {
        m_cp = cp;
    }
    
    public String getTitulo()
    {
        return this.m_strTitulo;
    }
    
    public void setTitulo(String strTitulo)
    {
        this.m_strTitulo = strTitulo;
    }

    public void setDescricao(String strDescricao)
    {
        this.m_strDescricao = strDescricao;
    }

    public void setDataInicio(String strDataInicio)
    {
        this.m_strDataInicio = strDataInicio;
    }

    public void setDataFim(String strDataFim)
    {
        this.m_strDataFim = strDataFim;
    }

    public void setLocal(String strLocal)
    {
        m_local.setLocal(strLocal);
    }

    public boolean addOrganizador(String strId, Utilizador u)
    {
        System.out.println("Evento: addOrganizador: " + strId + " - " + u.toString());
        Organizador o = new Organizador(strId, u);
        
        o.valida();
        
        return addOrganizador(o);
    }
    
    private boolean addOrganizador(Organizador o)
    {
        System.out.println("Evento: addOrganizador: " + o.toString());
        return true;
    }

    public boolean valida()
    {
        System.out.println("Evento:valida: " + this.toString());
        boolean bRet = m_local.valida();
        return bRet;
    }

    @Override
    public String toString()
    {
        return this.m_strTitulo + "+ ..."; 
    }
    
    public boolean aceitaSubmissoes() 
    {
        System.out.println("Evento:aceitaSubmissoes");
        return true;
    }

    public Submissao novaSubmissao() {
        return new Submissao();
    }
    
    public boolean addSubmissao(Submissao submissao)
    {
        if (validaSubmissao(submissao))
        {
            System.out.println("Evento:addSubmissao");
            return true;
        }
        else
            return false;
    }

    private boolean validaSubmissao(Submissao submissao) 
    {
        System.out.println("Evento:validaSubmissao");
        return submissao.valida();
    }
}
