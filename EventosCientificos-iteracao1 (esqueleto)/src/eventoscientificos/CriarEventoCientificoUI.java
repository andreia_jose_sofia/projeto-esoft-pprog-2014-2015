package eventoscientificos;

/**
 *
 * @author Nuno Silva
 */

public class CriarEventoCientificoUI
{
    private Empresa m_empresa;
    private CriarEventoCientificoController m_controllerCEC;

    public CriarEventoCientificoUI( Empresa empresa )
    {
        m_empresa = empresa;
        m_controllerCEC = new CriarEventoCientificoController(m_empresa);
    }

    
}