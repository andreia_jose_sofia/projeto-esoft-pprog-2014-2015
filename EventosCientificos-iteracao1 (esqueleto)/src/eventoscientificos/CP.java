package eventoscientificos;

/**
 *
 * @author Nuno Silva
 */

public class CP
{
    
    public CP()
    {
        
    }

    public Revisor addMembroCP( String strId, Utilizador u )
    {
        Revisor r = new Revisor(u);
        
        if( r.valida() && validaMembroCP(r) )
            return r;
        else
            return null;
    }
    
    private boolean validaMembroCP(Revisor r)
    {
        return true;
    }
    
    public boolean registaMembroCP(Revisor r)
    {
        System.out.println("CP: registaMembroCP:" + r.toString());
        return true;
    }
    
}