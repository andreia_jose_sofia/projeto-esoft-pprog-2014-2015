package eventoscientificos;

/**
 *
 * @author Nuno Silva
 */
public class CriarCPUI
{
    private Empresa m_empresa;
    private CriarCPController m_controllerCCP;

    public CriarCPUI( Empresa empresa )
    {
        m_empresa = empresa;
        m_controllerCCP = new CriarCPController(m_empresa);
    }
  
}