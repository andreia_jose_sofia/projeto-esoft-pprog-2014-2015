/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class Artigo 
{
    private String m_strTitulo;
    private String m_strResumo;
    private List<Autor> m_listaAutores;
    private Autor m_autorCorrespondente;
    private String m_strFicheiro;
    public Artigo()
    {
        m_listaAutores = new ArrayList<Autor>();
    }
    
    public void setTitulo(String strTitulo)
    {
        this.m_strTitulo = strTitulo;
    }
    
    public void setResumo(String strResumo)
    {
        this.m_strResumo = strResumo;
    }
    
    public Autor novoAutor(String strNome, String strAfiliacao)
    {
        Autor autor = new Autor();
        
        autor.setNome(strNome);
        autor.setAfiliacao(strAfiliacao);
        
        return autor;
    }
    
    public boolean addAutor(Autor autor)
    {
        if (validaAutor(autor))
            return m_listaAutores.add(autor);
        else
            return false;
    
    }

    private boolean validaAutor(Autor autor) 
    {
        return autor.valida();
    }
    
    public List<Autor> getPossiveisAutoresCorrespondentes()
    {
        List<Autor> la = new ArrayList<Autor>();
        
        for(Autor autor:this.m_listaAutores)
        {
           if (autor.podeSerCorrespondente())
           {
               la.add(autor);
           }    
        }
        return la;
    }
    
    public void setAutorCorrespondente(Autor autor)
    {
        this.m_autorCorrespondente=autor;
    }
    
    public void setFicheiro(String strFicheiro)
    {
        this.m_strFicheiro = strFicheiro;
    }
    
    public String getInfo()
    {
        System.out.println("Artigo:getInfo");
        return "Artigo:getInfo";
    }
    
    public boolean valida()
    {
        System.out.println("Artigo:valida");
        return true;
    }
 

}
